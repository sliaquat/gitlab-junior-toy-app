GitLab Junior is miniature version of GitLab that aims to be like GitLab when it grows up.

## Getting started

GitLab Junior Toy App is a Ruby on Rails application. You will need to install Ruby version specified
in the [.tool-versions](.tool-versions) file to run the App.

Once you have the correct version of Ruby installed, clone the repo and then install the needed gems:

```shell
$ gem install bundler
$ bundle
```

Next, migrate the database:

```shell
$ rails db:migrate
```

Seed the data:

```shell
$ rails db:seed
```

Finally, run the test suite to verify that everything is working correctly:

```shell
$ bundle exec rspec
```

If the test suite passes, you'll be ready to run the app in a local server:

```shell
$ rails server
```

At any time, if you would need to reset the db:

```shell
$ rails db:migrate:reset
```

## End-to-end test documentation

Head over to the [end-to-end test documentation section](qa/README.md)

## Application documentation

Head over to the [application's documentation section](docs/index.md)

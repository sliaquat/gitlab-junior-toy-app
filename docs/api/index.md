# GitLab Junior API Documentation

Welcome to the GitLab Junior REST API documentation!

If you are looking for UI documentation, [head over here](../index.md).

This application provides REST API for the the below mentioned resources. The API is versioned
with the current version at `1`.

API requests must include both api and the API version. The root of the v1 API is at /api/v1.

#### Valid API request

The following is a basic example of a request to the fictional `gitlabjunior.example.com` endpoint:

```shell
curl "http://gitlabjunior.example.com/api/v1/groups"
```

The API uses JSON to serialize data. You don’t need to specify .json at the end of the API URL.

#### Authentication

Most API requests require authentication.
When authentication is required, the documentation for each endpoint specifies this.
For example, the /groups/:id endpoint does not require authentication.

You can use Personal Access Tokens to authenticate with the API by passing it in the private_token parameter

Example of using the Personal Access Token in a parameter:

```shell
curl "http://gitlabjunior.example.com/api/v1/groups?private_token=<your_access_token>"
```

The application provides REST API for the following resources

## Users API

#### List Users

Get a list of users.

```plaintext
GET /users
```

```json
[
  {
    "id": 1,
    "name": "Admin User",
    "email": "admin@gitlabjunior.com",
    "web_url": "http://gitlabjunior.example.com/users/1"
  },
  {
    "id": 2,
    "name": "Elma Sauer",
    "email": "user-1@gitlabjunior.com",
    "web_url": "http://gitlabjunior.example.com/users/2"
  }
]
```

You can also use `?email=` to lookup a user by email. For example, `/users?email=user-1@gitlabjunior.com`.


| Parameter | Type   | Required | Description  |
|:----------|:-------|:---------|:-------------|
| `email`   | String | No       | User's email |

#### Singe User

Get a single user.

 ```plaintext
GET /users/:id
```

```json
{
  "id": 1,
  "name": "Admin User",
  "email": "admin@gitlabjunior.com",
  "web_url": "http://gitlabjunior.example.com/users/1"
}

```

| Parameter    | Type    | Required | Description  |
|:-------------|:--------|:---------|:-------------|
| `id`         | Integer | Yes      | User's id    |

#### Delete a User

Deletes a user. Available only for administrators.
This returns a `204 No Content` status code if the operation was successfully, `404` if the resource was not found.

Deleting a user also deletes all of it's groups and subgroups

A Personal Access Token is required for this endpoint.

 ```plaintext
DELETE /users/:id
```

| Parameter    | Type    | Required | Description           |
|:-------------|:--------|:---------|:----------------------|
| `id`         | Integer | Yes      | User's id             |

#### Create a new User

Creates a new user. Note only administrators can create new users.

A Personal Access Token is required for this endpoint.

```plaintext
POST /users
```

Parameters:

| Parameter               | Type    | Required | Description                                 |
|:------------------------|:--------|:---------|:--------------------------------------------|
| `email`                 | String  | Yes      | User's email. Must be a valid email format. |
| `name`                  | String  | Yes      | User's full name                            |
| `password`              | String  | Yes      | User's password                             |
| `password_confirmation` | String  | Yes      | Password confirmation                       |

## Groups API

Get a list of groups.

```plaintext
GET /groups
```

```json
[
  {
    "id": 1,
    "name": "My top level group",
    "description": "Lorem ipsum",
    "owner_id": 2,
    "parent_group_id": null,
    "web_url": "http://gitlabjunior.example.com/groups/1"
  },
  {
    "id": 2,
    "name": "My sub group",
    "description": "Lorem ipsum",
    "owner_id": 2,
    "parent_group_id": 1,
    "web_url": "http://gitlabjunior.example.com/groups/2"
  }
]
```

You can also use `?owner_id=` to fetch all groups belonging to a user . For example, `/users?owner_id=1`.

| Parameter  | Type    | Required | Description     |
|:-----------|:--------|:---------|:----------------|
| `owner_id` | Integer | No       | User's email    |

#### Single Group

Get a single group.

 ```plaintext
GET /groups/:id
```

```json
{
  "id": 1,
  "name": "My group",
  "description": "Lorem ipsum",
  "owner_id": 2,
  "parent_group_id": null,
  "web_url": "http://gitlabjunior.example.com/groups/1"
}

```

| Parameter    | Type    | Required | Description |
|:-------------|:--------|:---------|:------------|
| `id`         | Integer | Yes      | Group's id  |

#### Delete a Group

Deletes a group. Only the group's owner or an administrator can delete the group.
This returns a `204 No Content` status code if the operation was successfully, `404` if the resource was not found.

Deleting a group also deletes any subgroups under the group.

A Personal Access Token is required for this endpoint.

 ```plaintext
DELETE /users/:id
```

| Parameter    | Type    | Required | Description     |
|:-------------|:--------|:---------|:----------------|
| `id`         | Integer | Yes      | User's id       |

#### Sub groups

Get all subgroups of a group

 ```plaintext
GET /groups/:id/subgroups
```

```json
[
  {
    "id": 4,
    "name": "Sub group 1",
    "description": "Lorem ipsum",
    "owner_id": 2,
    "parent_group_id": 1,
    "web_url": "http://gitlabjunior.example.com/groups/1"
  },
  {
    "id": 5,
    "name": "Sub group 2",
    "description": "Lorem ipsum",
    "owner_id": 2,
    "parent_group_id": 1,
    "web_url": "http://gitlabjunior.example.com/groups/2"
  }
]
```

#### Create a new Group

Creates a new group. 

For creating a subgroup, provide the id of the parent group in the `parent_group_id` parameter.

A Personal Access Token is required for this endpoint.

```plaintext
POST /users
```

Parameters:

| Parameter            | Type    | Required | Description            |
|:---------------------|:--------|:---------|:-----------------------|
| `name`               | String  | Yes      | Group's name           |
| `description`        | String  | Yes      | Group's description    |
| `parent_group_id`    | String  | No       | Id of the parent group |

class GroupOwnerValidator < ActiveModel::Validator
  def validate(record)
    return unless record.parent_group

    unless record.owner == record.parent_group.owner
      record.errors.add :parent_group_id, "You are not allowed to create a subgroup for this group"
    end
  end
end

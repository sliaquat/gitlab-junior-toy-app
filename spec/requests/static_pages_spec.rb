require 'rails_helper'

RSpec.describe 'StaticPages', type: :request do
  let(:base_title){'GitLab Junior'}

  describe 'GET /home' do
    it 'returns http success' do
      get '/'
      expect(response).to have_http_status(:success)
      expect(response.body).to include("<title>#{base_title}</title>")
    end
  end

  describe 'GET /help' do
    it 'returns http success' do
      get '/help'
      expect(response).to have_http_status(:success)
      expect(response.body).to include("<title>Help | #{base_title}</title>")
    end
  end

  describe 'GET /about' do
    it 'returns http success' do
      get '/about'
      expect(response).to have_http_status(:success)
      expect(response.body).to include("<title>About | #{base_title}</title>")
    end
  end

  describe 'GET /contact' do
    it 'returns http success' do
      get '/contact'
      expect(response).to have_http_status(:success)
      expect(response.body).to include("<title>Contact | #{base_title}</title>")
    end
  end
end

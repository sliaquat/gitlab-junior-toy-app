require 'rails_helper'

RSpec.describe 'Users', type: :request do
  describe 'GET /new' do
    it 'returns http success' do
      get signup_path
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST /users' do
    let(:invalid_post_params) do
      { user: { name: '',
        email: 'user@invalid',
        password: 'foo',
        password_confirmation: 'bar' } }
    end

    let(:valid_post_params) do
      { user: { name: 'Some User',
        email: 'user@email.com',
        password: 'password',
        password_confirmation: 'password' } }
    end

    it 'does not save with invalid params' do
      expect { post users_path, params: invalid_post_params }.to_not change { User.count }
      expect(response).to have_http_status(:unprocessable_entity)
    end

    it 'saves with valid params' do
      expect { post users_path, params: valid_post_params }.to change { User.count }
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(user_path(User.last.id))
    end
  end

  describe 'GET /users' do
    let(:user) { create(:user) }
    let!(:users) { create_list(:user, 30) }

    it 'redirects to index when not logged in' do
      get users_path
      expect(response).to redirect_to(login_url)
    end

    it 'paginates users' do
      log_in_as(user)
      get users_path

      User.paginate(page: 1).each do |user|
        expect(response.body).to include("<a href=\"#{user_path(user)}\">#{user.name.gsub('\'', '&#39;')}</a>")
      end
    end
  end

  describe 'DELETE /users' do
    let(:user) { create(:user) }
    let(:admin_user) { create(:user, :admin) }

    it 'redirects to index when not logged in' do
      user
      expect { delete user_path(user) }.to_not change { User.count }
      expect(response).to have_http_status(:see_other)
      expect(response).to redirect_to(login_url)
    end

    it 'deletes user when logged in as admin' do
      log_in_as(admin_user)
      user
      expect { delete user_path(user) }.to change { User.count }.by(-1)
      expect(response).to have_http_status(:see_other)
    end
  end
end

def log_in_as(user, password: 'password', remember_me: '1')
  post login_path, params: { session: { email: user.email,
    password: password,
    remember_me: remember_me } }
end

module QA
  module Resource
    class User < Base
      attributes :id,
                 :name,
                 :email,
                 :password,
                 :admin

      def fabricate_via_browser_ui!
        QA::Page::Signup.perform do |signup|
          signup.fill_name_field(name)
          signup.fill_email_field(email)
          signup.fill_password_field(password)
          signup.fill_password_confirmation_field(password)
          signup.click_signup_button
        end
      end

      def admin?
        api_resource&.dig(:admin) || false
      end

      def fabricate_via_api!
        resource_web_url(api_get)
      rescue ResourceNotFoundError
        super
      end

      def exists?
        api_get
      rescue ResourceNotFoundError
        false
      end

      def name
        @name ||= FFaker::Name.name
      end

      def password
        @password ||= 'Pa$$w0rd'
      end

      def email
        @email ||= begin
                     api_email = api_resource&.dig(:email)
                     api_email && !api_email.empty? ? api_email : "qa-user-#{SecureRandom.hex(4)}@example.com"
                   end
      end

      def api_get_path
        "/users/#{@id ||= fetch_id(email)}"
      end

      def api_post_path
        '/users'
      end

      def api_post_body
        {
          name: name,
          email: email,
          password: password,
          password_confirmation: password,
        }
      end

      def api_delete_path
        api_get_path
      end

      private

      def fetch_id(email)
        users = parse_body(api_get_from("/users?email=#{email}"))

        unless users.size == 1 && users.first[:email] == email
          raise ResourceNotFoundError, "Expected one user with email #{email} but found: `#{users}`."
        end

        users.first[:id]
      end

    end
  end
end

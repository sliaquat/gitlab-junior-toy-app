require 'capybara/dsl'
require 'ffaker'

module QA
  module Resource
    class Base
      include ApiFabricator
      extend Capybara::DSL

      NoValueError = Class.new(RuntimeError)

      attr_accessor :web_url, :fabrication_success_text

      def fabricate!
        raise NotImplementedError
      end

      def perform_post_ui_fabrication_hook; end

      def visit!
        visit(web_url)
      end

      def populate(*attribute_names)
        attribute_names.each { |attribute_name| public_send(attribute_name) }
      end

      class << self
        # Initialize new instance of class without fabrication
        #
        # @param [Proc] prepare_block
        def init(&prepare_block)
          new.tap(&prepare_block)
        end

        def fabricate!(&prepare_block)
          fabricate_via_browser_ui!(&prepare_block)
        end

        def fabricate_via_browser_ui!(&prepare_block)
          resource = new

          do_fabricate!(resource: resource, prepare_block: prepare_block) do
            resource.fabricate_via_browser_ui!

            if resource.fabrication_success_text && !has_text?(resource.fabrication_success_text)
              raise(
                QA::Resource::Errors::ResourceFabricationFailedError,
                "Fabrication of #{resource.class.name} via the UI failed."
              )
            end

            resource.perform_post_ui_fabrication_hook

            current_url
          end
        end

        def fabricate_via_api!(&prepare_block)
          resource = new

          raise NotImplementedError unless resource.api_support?

          resource.eager_load_api_client!

          do_fabricate!(resource: resource, prepare_block: prepare_block) do
            resource.fabricate_via_api!
          end
        end

        private

        def do_fabricate!(resource:, prepare_block:)
          prepare_block.call(resource) if prepare_block

          resource_web_url = yield
          resource.web_url = resource_web_url

          resource
        end

        # Define custom attribute
        #
        # @param [Symbol] name
        # @return [void]
        def attribute(name, &block)
          (@attribute_names ||= []).push(name) # save added attributes

          attr_writer(name)

          define_method(name) do
            return instance_variable_get("@#{name}") if instance_variable_defined?("@#{name}")

            instance_variable_set("@#{name}", attribute_value(name, block))
          end
        end

        # Define multiple custom attributes
        #
        # @param [Array] names
        # @return [void]
        def attributes(*names)
          names.each { |name| attribute(name) }
        end
      end

      private

      def attribute_value(name, block)
        no_api_value = !api_resource&.key?(name)
        raise NoValueError, "No value was computed for #{name} of #{self.class.name}." if no_api_value && !block

        unless no_api_value
          api_value = api_resource[name]
          return api_value
        end

        instance_exec(&block)
      end
    end
  end
end

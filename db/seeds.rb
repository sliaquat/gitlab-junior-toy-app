# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Create a main sample user.
admin_user = User.create!(name: "Admin User",
  email: "admin@gitlabjunior.com",
  password: "5iveL!fe",
  password_confirmation: "5iveL!fe",
  admin: true)

admin_user.personal_access_tokens.create!(name: "Admin Personal Access Token", value: 'f94dc3ff597d7b2ddd0ae08a63e1f3f8')

# Generate a bunch of additional users.
40.times do |n|
  name = FFaker::Name.name
  email = "user-#{n + 1}@gitlabjunior.com"
  password = "password"
  User.create!(name: name,
    email: email,
    password: password,
    password_confirmation: password)
end
